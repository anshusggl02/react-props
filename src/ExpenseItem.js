import React from "react";

import "./ExpenseItem.css";

const ExpenseItem = (props) => {
  return (
    <div className="main-div">
      <div className="expense-item"> {props.title} </div>
      <div className="expense-item-name"> {props.price}</div>
      <div className="expense-item-price"> {props.item} </div>
    </div>
  );
};
export default ExpenseItem;
