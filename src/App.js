import React, { useState } from "react";

//import TestComponent from "./components/TestComponent";
//import Item from "./ExpenseItem";
import ExpenseForm from "./components/NewExpense/ExpenseForm";

const App = () => {
  const [expense, setExpense] = useState([]);

  const addNewExpenseItem = (expenseItem) => {
    if (expense.length) {
      setExpense((prev) => {
        return [...prev, expenseItem];
      });
    } else {
      setExpense([expenseItem]);
    }
  };

  return (
    <div>
      <ExpenseForm addNewExpenseItem={addNewExpenseItem} />
      {expense.length
        ? expense.map((item, index) => {
            return (
              <div key={index}>
                {item.title}
                <br />
                {item.price}
                <br />
                {item.item}
                <br />
              </div>
            );
          })
        : ""}
    </div>
  );
};
export default App;

//https://gitlab.com/anshusggl02/react-props
