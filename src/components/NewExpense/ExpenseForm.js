import React, { useState } from "react";

import "./ExpenseForm.css";

function ExpenseForm(props) {
  const [expenseItem, setExpenseItem] = useState({
    title: " ",
    price: " ",
    item: " ",
  });

  const SubmitForm = (event) => {
    event.preventDefault();

    props.addNewExpenseItem(expenseItem);
  };

  const changeTitle = (event) => {
    setExpenseItem((prev) => {
      prev.title = event.target.value;
      return prev;
    });
  };

  const changePrice = (event) => {
    setExpenseItem((prev) => {
      prev.price = event.target.value;
      return prev;
    });
  };

  const changeItem = (event) => {
    setExpenseItem((prev) => {
      prev.item = event.target.value;
      return prev;
    });
  };

  return (
    <form onSubmit={SubmitForm}>
      <div className="new-expense">
        <div className="new-expense">
          <label> Title </label>
          <input type="text" onChange={changeTitle} />
        </div>
        <div className="new-expense">
          <label> Amount </label>
          <input type="number" onChange={changePrice} />
        </div>
        <div className="new-expense">
          <label> Item </label>
          <input type="text" onChange={changeItem} />
        </div>
      </div>
      <div className="new-expense-control">
        <button type="submit">Add expense</button>
      </div>
    </form>
  );
}

export default ExpenseForm;
