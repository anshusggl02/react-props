import React, { useState } from "react";

const TestComponent = ({ value }) => {
  const [name, setName] = useState("");

  const handleOnClick = () => {
    setName(value);
    alert("click here " + value);
  };

  React.useEffect(() => {
    console.log("check the name", name);
  }, [name]);

  return <button onClick={handleOnClick}>{value}</button>;
};

export default TestComponent;
